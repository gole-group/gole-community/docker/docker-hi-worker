FROM ansible/ansible:fedora27py3

RUN yum install -y curl nmap sshpass iputils net-snmp-libs krb5-workstation
    
WORKDIR /opt

ARG BRANCH=master

ARG URL=https://gitlab.com/gole-group/core/hi-worker/-/archive/$BRANCH/hi-worker-$BRANCH.tar.gz
RUN curl -o - "${URL}" | \
    tar xz ; mv hi-worker-* hi-worker
    
WORKDIR /opt/hi-worker
RUN rpm -Uhv rpm/wmic-4.0.0tp4-0.x86_64.rpm && \
    pip3 install -r requirements.txt && \
    rm -rf rpm && yum clean all && \
    cp wrapper.py /usr/local/lib/python3.6/site-packages/wmi_client_wrapper/ 
    
CMD [ "gunicorn", "-c", "gunicorn_config.py", "workerapi:app" ]
