 python-nmap-rest-api
======================

Components needed to build *python-nmap-rest-api* as a Docker container.

* [Source code](https://gitlab.com/gole-group/core/python-nmap-rest-api)

* [Official Documentation](https://python-nmap-rest-api.readthedocs.io/en/latest/)

You can see the official docker images on [Hub Docker](https://hub.docker.com/r/gole/python-nmap-rest-api)

## Docker Run

The easy way to put the python-nmap-rest-api working:

Temporary:
```
$ docker run -ti --rm --name python-nmap-rest-api -p 8001:8001 \
  gole/python-nmap-rest-api:latest
```

Background:
```
$ docker run -d --name python-nmap-rest-api -p 8001:8001 \
  gole/python-nmap-rest-api:latest
$ curl http://localhost:8001
{"hello":"nmap world"}
```

## Build Docker from source

If you need change the docker image, you can build from source:

```
$ git clone -b master https://gitlab.com/gole-group/gole-community/docker/python-nmap-rest-api.git
$ cd python-nmap-rest-api
$ docker-compose up -d
$ curl http://localhost:8001
{"hello":"nmap world"}
```
